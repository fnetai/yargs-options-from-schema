# @fnet/yargs-options-from-schema

This project provides a utility for converting JSON schema definitions into yargs-compatible options objects. It facilitates command-line argument parsing by automatically mapping schema attributes, such as types, descriptions, and default values, to yargs configuration options.

## How It Works

The tool takes a JSON schema as input and processes its properties to generate an object suitable for use with yargs' `.options()` method. It supports various schema attributes, including type conversion and description, and handles nested properties and `oneOf` constructs to define mutually exclusive options in yargs.

## Key Features

- Converts JSON schema to yargs options format.
- Supports nested properties and complex schema constructs.
- Handles type conversion for yargs compatibility.
- Supports `oneOf` schemas by establishing conflict relationships.
- Includes descriptions and default values when present in the schema.
- Allows for predefined choices and aliases in yargs options.

## Conclusion

This utility is particularly useful for developers looking to leverage JSON schemas to streamline the setup of command-line interfaces using yargs. It automates the tedious task of manually defining options by interpreting schema attributes and applying them directly to yargs configuration.