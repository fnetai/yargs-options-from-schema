import fnetYaml from '@fnet/yaml';

/**
 * Converts a JSON schema into an object suitable for yargs.options, supporting a wide range of attributes.
 *
 * This function aims to provide a comprehensive mapping from JSON schema properties to yargs option attributes,
 * including type conversion, descriptions, default values, choice constraints, aliases, and more.
 * Supports nested properties by using recursive processing for deeply nested objects.
 *
 * @param {Object} args - The function arguments.
 * @param {Object} args.schema - The JSON schema object to convert.
 * @returns {Object} An options object compatible with yargs.options, enriched with a broad set of attributes.
 */
export default async ({ schema }) => {
  
  const processedSchema = await loadSchema(schema);
  
  return processProperties(processedSchema.properties, processedSchema.required || []);
};

function processProperties(properties, required = [], parentKey = '') {
  const options = {};

  for (const [key, value] of Object.entries(properties)) {
    // Full key path for nested properties
    const fullKey = parentKey ? `${parentKey}.${key}` : key;

    // Check if the property has a oneOf structure
    if (value.oneOf) {
      processOneOf(value.oneOf, options, fullKey);
      continue; // Skip further processing for oneOf
    }

    // If the type is 'object', process nested properties recursively
    if (value.type === 'object' && value.properties) {
      Object.assign(options, processProperties(value.properties, value.required || [], fullKey));
    } else {
      const option = {
        type: convertType(value.type),
        demandOption: required.includes(key) || false,
        describe: value.description?.trim() || undefined,
      };

      // Only add default if it's explicitly defined
      if (typeof value.default !== 'undefined') {
        option.default = value.default;
      }

      // Supporting enum for predefined choices
      if (value.enum) {
        option.choices = value.enum;
      }

      // Supporting aliases for alternate option names
      if (value.alias) {
        option.alias = value.alias;
      }

      options[fullKey] = option;
    }
  }

  return options;
}

/**
 * Processes a oneOf structure in JSON schema.
 * This creates conflict relationships between the different oneOf options.
 *
 * @param {Array} oneOfOptions - The oneOf schema definitions.
 * @param {Object} options - The yargs options object to be populated.
 * @param {String} parentKey - The parent key for nested properties.
 */
function processOneOf(oneOfOptions, options, parentKey = '') {
  const keys = [];

  oneOfOptions.forEach((oneOfItem) => {
    // Check if the oneOf item has properties
    if (oneOfItem.properties) {
      const oneOfProperties = oneOfItem.properties;
      const oneOfKeys = Object.keys(oneOfProperties).map((key) =>
        parentKey ? `${parentKey}.${key}` : key
      );
      keys.push(...oneOfKeys);

      // Process each oneOf item's properties as normal
      Object.assign(
        options,
        processProperties(oneOfProperties, oneOfItem.required || [], parentKey)
      );
    } else {
      // Handle case where oneOf item doesn't have properties (e.g., type: string)
      const option = {
        type: convertType(oneOfItem.type),
        describe: oneOfItem.description?.trim() || undefined,
      };

      if (typeof oneOfItem.default !== 'undefined') {
        option.default = oneOfItem.default;
      }

      options[parentKey] = option;
    }
  });

  // Add conflicts between each oneOf option
  keys.forEach((key, index) => {
    options[key].conflicts = keys.filter((_, i) => i !== index);
  });
}

/**
 * Converts a JSON schema data type to a yargs option type.
 *
 * @param {string} type - The JSON schema data type.
 * @returns {string} The corresponding yargs option type.
 */
function convertType(type) {
  const typeMapping = {
    string: 'string',
    number: 'number',
    integer: 'number',
    boolean: 'boolean',
    array: 'array',
  };

  return typeMapping[type] || 'string';
}

/**
 * Determines whether the schema source is a local file path, a URL, or a direct object.
 * If the source is a string, it attempts to load the content as YAML or JSON from the given file path or URL.
 * 
 * @param {string|Object} source - The schema source to be loaded.
 * @returns {Promise<Object>} The loaded and parsed schema object.
 */
async function loadSchema(source) {
  if (typeof source === 'object') {
    // Direct object schema, return as is.
    return source;
  } else if (typeof source === 'string') {
    try {
      const { parsed } = await fnetYaml({ file: source });
      return parsed;
    } catch (error) {
      console.error(`Error loading schema from ${source}:`, error);
      throw error;
    }
  } else {
    throw new Error('Unsupported schema format');
  }
}