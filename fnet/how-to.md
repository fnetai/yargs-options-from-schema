# Developer Guide for @fnet/yargs-options-from-schema

## Overview

The `@fnet/yargs-options-from-schema` library converts a JSON schema into an options object compatible with `yargs`. This allows developers to define command-line argument parsers with comprehensive mappings from JSON schema attributes. The library supports conversion for type definitions, default values, descriptions, choices (enums), and aliases, among other features. It also processes nested properties and `oneOf` structures to offer flexible and conflict-managed command-line option setups.

## Installation

To install the `@fnet/yargs-options-from-schema` library, use either npm or yarn:

```bash
npm install @fnet/yargs-options-from-schema
```

or

```bash
yarn add @fnet/yargs-options-from-schema
```

## Usage

The library's primary functionality is encapsulated in a default exported function that takes a JSON schema and returns a yargs-compatible options object. Here's how to use it in a typical scenario:

```javascript
import optionsFromSchema from '@fnet/yargs-options-from-schema';

// Sample JSON schema
const schema = {
  properties: {
    username: {
      type: 'string',
      description: 'The user name',
      default: 'guest',
    },
    port: {
      type: 'number',
      description: 'Port number for the server',
      default: 8080,
    },
    verbose: {
      type: 'boolean',
      description: 'Enable verbose logging',
    },
  },
  required: ['username'],
};

// Conversion to yargs options
const yargsOptions = await optionsFromSchema({ schema });

// Use with yargs
import yargs from 'yargs';

yargs
  .options(yargsOptions)
  .help()
  .argv;
```

## Examples

### Basic Usage

```javascript
// JSON schema with basic data types
const basicSchema = {
  properties: {
    name: {
      type: 'string',
    },
    age: {
      type: 'number',
    },
    isAdmin: {
      type: 'boolean',
      default: false,
    }
  }
};

// Convert schema to yargs options
const options = await optionsFromSchema({ schema: basicSchema });
```

### Nested Properties and `oneOf` Usage

```javascript
// JSON schema with nested properties and oneOf
const complexSchema = {
  properties: {
    server: {
      type: 'object',
      properties: {
        host: {
          type: 'string',
          default: 'localhost',
        },
        port: {
          type: 'number',
          default: 3000,
        },
      },
    },
    database: {
      oneOf: [
        {
          properties: {
            type: {
              type: 'string',
              enum: ['sql', 'nosql'],
            },
            connection: {
              type: 'string',
            },
          },
          required: ['type'],
        },
      ],
    },
  },
};

// Convert schema to yargs options
const complexOptions = await optionsFromSchema({ schema: complexSchema });
```

These code examples demonstrate converting JSON schema into yargs-compatible option configurations, including complex setups with nested properties and conditional options.

## Acknowledgement

This library utilizes `@fnet/yaml` for schema parsing from YAML files. The contributors to this library have aimed to streamline command-line configuration using schema-driven approaches.